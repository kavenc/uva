#include <algorithm>
#include <iostream>
#include <utility>
#include <vector>

static int NumDim = 0;
static int NumBoxes = 0;
// {
//   [Dim0, Dim1, Dim2, ..., BoxIndex],
//   [Dim0, Dim1, Dim2, ..., BoxIndex],
//   ...
// }
static std::vector<std::vector<int>> Boxes(30, std::vector<int>(10 + 1, 0));

// {{Len, Next}, ...}
static std::vector<std::pair<int, int>> Path(30);

static inline bool Fits(const std::vector<int> &Small,
                        const std::vector<int> &Big, int Len) {
  auto SmallIter = Small.begin();
  auto BigIter = Big.begin();
  while (Len--) {
    if (*(SmallIter++) >= *(BigIter++)) {
      return false;
    }
  }
  return true;
}

static inline void LongestStringFixedStart(int Start, int &MaxLen) {
  const auto *SmallBox = &Boxes[Start];
  int LocalMax = 1;
  for (auto TestIndex = Start + 1; NumBoxes - TestIndex + 1 > LocalMax;
       ++TestIndex) {
    int NextMax = 0;
    if (Fits(*SmallBox, Boxes[TestIndex], NumDim)) {
      const auto &PathInfo = Path[TestIndex];
      NextMax = PathInfo.first;
      if (NextMax < 0) {
        LongestStringFixedStart(TestIndex, NextMax);
      }

      if (NextMax + 1 > LocalMax) {
        LocalMax = NextMax + 1;
        Path[Start].first = LocalMax;
        Path[Start].second = TestIndex;
      }
    }
  }
  MaxLen = LocalMax;
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);

  while (std::cin >> NumBoxes) {
    std::cin >> NumDim;
    auto Box = Boxes.begin();
    auto PathIter = Path.begin();

    // Read inputs
    for (int BoxIndex = 1; BoxIndex <= NumBoxes; ++BoxIndex, ++Box) {
      auto Dim = Box->begin();
      for (int DimIndex = 0; DimIndex < NumDim; ++DimIndex, ++Dim) {
        std::cin >> *Dim;
      }
      *Dim = BoxIndex;

      // Sort intra boxes
      std::sort(Box->begin(), Box->begin() + NumDim);

      // Reset path structures
      PathIter->first = -1;
      ++PathIter;
    }

    // Sort inter boxes
    std::sort(Boxes.begin(), Boxes.begin() + NumBoxes);

    // Calculate longest string
    int MaxLength = 1;
    int MaxStart = 0;
    for (auto TestStart = 0; NumBoxes - TestStart + 1 > MaxLength;
         ++TestStart) {
      int Len;
      LongestStringFixedStart(TestStart, Len);
      if (Len > MaxLength) {
        MaxLength = Len;
        MaxStart = TestStart;
      }
    }

    // Output
    std::cout << MaxLength << "\n" << Boxes[MaxStart][NumDim];
    int Next = Path[MaxStart].second;
    while (--MaxLength) {
      std::cout << " " << Boxes[Next][NumDim];
      Next = Path[Next].second;
    }
    std::cout << "\n";
  }
}