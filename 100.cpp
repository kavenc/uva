#include <algorithm>
#include <iostream>
#include <map>

int main() {
  int Start, End;
  std::map<int, int> Cache;
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  while (std::cin >> Start) {
    std::cin >> End;
    std::cout << Start << " " << End << " ";
    if (End < Start) {
      std::swap(Start, End);
    }
    int Max = 0;
    for (int Check = Start; Check <= End; Check++) {
      int Ans = 1;
      auto Find = Cache.lower_bound(Check);
      if (Find != Cache.end() && Find->first == Check) {
        Ans = Find->second;
      } else {
        int Calc = Check;
        while (Calc != 1) {
          Calc = Calc % 2 ? 3 * Calc + 1 : Calc / 2;
          ++Ans;
        }
        Cache.insert(Find, std::make_pair(Check, Ans));
      }
      Max = std::max(Max, Ans);
    }
    std::cout << Max << "\n";
  }
  return 0;
}