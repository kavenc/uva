#include <cctype>
#include <iostream>
#include <iterator>
#include <list>
#include <string>

using namespace std;

int main() {
  std::list<string> Dict;
  std::string Word;
  char ReadIn;
  ios_base::sync_with_stdio(false);
  cin.tie(nullptr);
  int Index = 0;
  while (cin.get(ReadIn)) {
    if (std::isdigit(ReadIn)) {
      if (Index == 0 && ReadIn == '0') {
        break;
      }
      Index = Index * 10 + ReadIn - '0';
    } else {
      if (std::isalpha(ReadIn)) {
        Word.push_back(ReadIn);
        continue;
      }

      if (Index != 0) {
        const auto Match = std::next(Dict.begin(), Index - 1);
        Dict.emplace_front(std::move(*Match));
        Dict.erase(Match);
        cout << Dict.front();
        Index = 0;
      } else if (!Word.empty()) {
        Dict.push_front(Word);
        cout << Word;
        Word.clear();
      }
      cout << ReadIn;
    }
  }
  return 0;
}