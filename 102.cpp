#include <iostream>

constexpr int Map[6][3] = {
    {0, 5, 7}, {0, 4, 8}, {2, 3, 7}, {2, 4, 6}, {1, 3, 8}, {1, 5, 6},
};

constexpr char Pattern[6][4] = {
    "BCG", "BGC", "CBG", "CGB", "GBC", "GCB",
};

int main() {
  unsigned Bins[9];
  std::cin.tie(nullptr);
  while (std::cin >> Bins[0]) {
    int Sum = Bins[0];
    for (int I = 1; I < 9; ++I) {
      std::cin >> Bins[I];
      Sum += Bins[I];
    }

    int Move;
    int Min = -1;
    int Index = 0;

    for (int I = 0; I < 6; ++I) {
      Move = Sum;
      for (auto SI : Map[I]) {
        Move -= Bins[SI];
      }
      if (Min == -1 || Move < Min) {
        Min = Move;
        Index = I;
      }
    }
    std::cout << Pattern[Index] << " " << Min << "\n";
  }

  return 0;
}